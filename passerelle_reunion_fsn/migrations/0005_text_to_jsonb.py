from django.db import migrations
from passerelle.utils.db import EnsureJsonbType


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_reunion_fsn', '0004_wcs_batches'),
    ]

    operations = [
        EnsureJsonbType(model_name='FSNReunionConnector', field_name='wcs_options'),
    ]
