# Generated by Django 1.11.18 on 2020-04-24 15:37

import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models

import passerelle_reunion_fsn.models


class Migration(migrations.Migration):

    dependencies = [
        ('passerelle_reunion_fsn', '0003_csv_checksum'),
    ]

    operations = [
        migrations.CreateModel(
            name='Batch',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                ('batch_date', models.DateField()),
                ('last_update_datetime', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ('batch_date',),
            },
        ),
        migrations.CreateModel(
            name='BatchFile',
            fields=[
                (
                    'id',
                    models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID'),
                ),
                (
                    'csv_file',
                    models.FileField(upload_to=passerelle_reunion_fsn.models.batch_csv_file_location),
                ),
                ('csv_filename', models.CharField(max_length=256)),
                ('ready', models.BooleanField(default=False)),
                ('last_update_datetime', models.DateTimeField(auto_now=True)),
                (
                    'batch',
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name='files',
                        to='passerelle_reunion_fsn.Batch',
                    ),
                ),
            ],
        ),
        migrations.AddField(
            model_name='fsnreunionconnector',
            name='wcs_form_slug',
            field=models.CharField(blank=True, max_length=256, verbose_name='WCS form slug'),
        ),
        migrations.AddField(
            model_name='fsnreunionconnector',
            name='wcs_options',
            field=django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='batch',
            name='resource',
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name='batches',
                to='passerelle_reunion_fsn.FSNReunionConnector',
            ),
        ),
        migrations.AlterUniqueTogether(
            name='batch',
            unique_together={('resource', 'batch_date')},
        ),
    ]
