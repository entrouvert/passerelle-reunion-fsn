import django_webtest
import pytest
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from passerelle.base.models import AccessRight, ApiUser

from passerelle_reunion_fsn.models import FSNReunionConnector


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    cache.clear()
    yield django_webtest.DjangoTestApp()
    wtm._unpatch_settings()


@pytest.fixture
def connector(db):
    connector = FSNReunionConnector.objects.create(
        slug='test', api_url='https://whatever', token='token', demarche_number=1, instructeur_id='xxxx'
    )
    api = ApiUser.objects.create(username='all', keytype='', key='')
    obj_type = ContentType.objects.get_for_model(connector)
    AccessRight.objects.create(
        codename='can_access', apiuser=api, resource_type=obj_type, resource_pk=connector.pk
    )


def test_dummny(app, connector):
    assert True
