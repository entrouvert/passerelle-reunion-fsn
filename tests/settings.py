import os

INSTALLED_APPS += ('passerelle_reunion_fsn',)


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'TEST': {
            'NAME': 'passerelle-reunion-fsn-test-%s'
            % os.environ.get('BRANCH_NAME', '').replace('/', '-')[:63],
        },
    }
}
